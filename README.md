# DireGaming Test

This test has 2 part, front-end and back-end. Please navigate to:
* `weather-app`: to see my front-end submission
* `DireGamingBackEnd`: to see my back-end submission

## Time Allocated
* `Front-end`: 1 day
* `Back-end`: 1.5 days
