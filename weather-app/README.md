
# Weather App

This application is to grab the current data from https://openweathermap.org/current and display as a tiles list with the chosen cities. The cities can also be removed by simply clicking at the `Remove` button.

## Run the App

In the project directory, run the app locally with `npm start` and open [http://localhost:3000](http://localhost:3000) to view it in the browser.
